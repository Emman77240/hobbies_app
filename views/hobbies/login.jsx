import React, { Component } from "react";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      statusMessage: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    if (typeof statusMessage !== 'undefined') {
      this.state.statusMessage = statusMessage;
    }

    return (
      <div className="limiter">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
                crossOrigin="anonymous" />
        <link rel="stylesheet" href="/styles/main.css" />

        <title>Login : GoHobbies App</title>
        <link rel="stylesheet" href="/styles/static/css/main.css" />
        <nav className="navbar navbar-toggleable-md navbar-light">
        <div className="navbar-collapse collapse">
          <ul className="nav navbar-nav">
            <li><a href="/">GoHobbies App</a></li>
          </ul>
        </div>
        </nav> 
        <body background="https://www.thoughtco.com/thmb/EhTrbae6AAZQyvi_cyXWeawBfqg=/1200x800/filters:fill(auto,1)/tax2_image_hobbies_activities-58a22d1468a0972917bfb54e.png" />
        <style type="text/css" /> 
        <div className="jumbotron">
          <center>
          <div className="container-login100">
            <div className="wrap-login100">
              <form method="post" action="" className="login100-form validate-form">
                <span className="login100-form-title">
                  Member Login
                </span>
                <div className="wrap-input100 validate-input" data-validate="Valid username is required">
                  <input className="input100" type="text" name="username" placeholder="Username" />
                  <span className="focus-input100"></span>
                  <span className="symbol-input100">
                    <i className="fa fa-envelope" aria-hidden="true"></i>
                  </span>
                </div>
                <div className="wrap-input100 validate-input" data-validate="Password is required">
                  <input className="input100" type="password" name="password" placeholder="Password" />
                  <span className="focus-input100"></span>
                  <span className="symbol-input100">
                    <i className="fa fa-lock" aria-hidden="true"></i>
                  </span>
                </div>
                <div className="container-login100-form-btn">
                  <button className="login100-form-btn">
                    Login
                  </button>
                </div>
                <br />
                <div className="text-center">
                  <p><b>{this.state.statusMessage}</b></p>
                </div>
                <div className="text-center p-t-136">
                  <a className="txt2" href="/register">
                    Create your Account
                    <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                  </a>
                </div>
              </form>
            </div>
          </div>
          </center>
        </div>
        <script>
          if(performance.navigation.type == 1) {
            this.state.statusMessage = ''
          }
        </script>
      </div>
    );
  }
}