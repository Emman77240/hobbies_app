import React, { Component } from "react";

export default class Login extends Component {
    constructor(props) {
        super(props);
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }



    render() {

        return (
            <div className="limiter">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
                crossOrigin="anonymous" />
                <link rel="stylesheet" href="/styles/main.css" />
                <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
                <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  
                <title>Home: GoHobbies App</title>
                <link rel="stylesheet" href="/styles/static/css/main.css" />
                <nav className="navbar navbar-toggleable-md navbar-light">
                

                <div className="navbar-collapse collapse">
                    <ul className="nav navbar-nav">
                        <li><a href="/">GoHobbies App</a></li>
                    </ul>
                </div>

                </nav>
                <body background="https://www.thoughtco.com/thmb/EhTrbae6AAZQyvi_cyXWeawBfqg=/1200x800/filters:fill(auto,1)/tax2_image_hobbies_activities-58a22d1468a0972917bfb54e.png" />
                <style type="text/css" />
                <div className="jumbotron">
                    <div className="container">
                        <center><h1 className="display-3">Welcome to GoHobbies!</h1></center>
                        <hr/>
                        <center><h4><small>Sign up to log your <strong>pasatiempo's</strong> (this means hobbies too...)</small></h4></center>
                        <center>
                            <div>
                                <form className="login100-form validate-form">
                                    <a href="/login" className="btn btn-warning btn-lg">Login</a>
                                    <a href="/register" className="btn btn-danger btn-lg">Register</a>
                                </form>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        );
    }
}