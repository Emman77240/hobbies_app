import React, { Component } from "react";

export default class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
        };
    }


    render() {
        console.log(message);
        return (
        <div className="limiter">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
                crossOrigin="anonymous" />
        <link rel="stylesheet" href="/styles/main.css" />

        <title>Dashboard : GoHobbies App</title>
        <link rel="stylesheet" href="/styles/static/css/main.css" />
        <nav className="navbar navbar-toggleable-md navbar-light">

        <div className="navbar-collapse collapse">
          <ul className="nav navbar-nav">
            <li><a href="/logout">Log Out</a></li>
          </ul>
        </div>
        </nav>
        <body background="https://www.thoughtco.com/thmb/EhTrbae6AAZQyvi_cyXWeawBfqg=/1200x800/filters:fill(auto,1)/tax2_image_hobbies_activities-58a22d1468a0972917bfb54e.png" />
                        
        <div className="jumbotron">  
            <h3>Hi, Welcome to your GoHobbies Dashboard</h3>
            <hr />
            <form id="hobbyForm" method="POST">
                <div className="form-group">
                    <p>
                        <input id="hobbyText" className="form-control" type="text" name="title" placeholder="Enter Hobby" required />
                        <input type="hidden" value={userId} name="owner" />
                    </p>
                    <br />
                    <button className="btn btn-success" type="submit">
                        <span className="glyphicon glyphicon-plus-sign"></span>
                        Add Hobby
                    </button>
                </div>
            </form>
            <div>
                <h4 id="message"></h4>
                <h4 id="message2">{message}</h4>
            </div>
        </div>
            <div>
                <script>
                    if(performance.navigation.type == 1) {
                        message = ''
                    }
                </script>

            </div >
        </div>
        );
    }
}