

module.exports = {

	createUser: function (req, res) {
		User.findOrCreate({
			username: req.body.username,
			email: req.body.email,
		}, {
			username: req.body.username,
			email: req.body.email,
			phone: req.body.phone,
			password: req.body.password
		}, (err, existingUser, newUser) => {
			if (err) statusMessage = 'ERROR ADDING USER';
			if (existingUser) statusMessage = 'USER ALREADY EXISTS';
			if (newUser) {
				statusMessage = 'USER SUCCESSFULLY ADDED';
			}
			res.redirect('/login');
		})
	}
};