const passport = require('passport');

module.exports = {

  login: function (req, res) {
    passport.authenticate('local', (err, user, info) => {
      if ((err) || (!user)) {
        statusMessage = 'Wrong Credentials';
        return res.redirect('back');
      }

      message = '';
      userId = user.id;
      req.session.userId = user.id;
      username = user.username;
      req.session.username = user.username;
      email = user.email;
      req.session.email = user.email;
      phone = user.phone;
      req.session.phone = user.phone;
      statusMessage = '';
      res.redirect('dashboard');

    })(req, res);

  },

  logout: function (req, res) {
    req.session.destroy();
    req.logout();
    res.redirect('/login');
  }

};