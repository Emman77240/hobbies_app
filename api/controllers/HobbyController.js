require('dotenv').config();

const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SG_KEY);


const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_TOKEN;
const client = require('twilio')(accountSid, authToken);


module.exports = {

  addhobby: function (req, res) {
    message = '';
    if (req.body.title.trim().length !== 0 && req.body.owner.trim().length !== 0) {
      Hobby.findOrCreate({
        title: req.body.title,
        owner: req.body.owner
      }, {
        title: req.body.title.trim(),
        owner: req.body.owner.trim()
      }, (err, existingHobby, newHobby) => {
        if (err) { 
          message = 'Hobby could not be added';
          }

        if (existingHobby) { 
          message = `You have added ${req.body.title} previously`
          }

        if (newHobby) {
          message = `New Hobby Added : ${req.body.title}`;

          const msg = {
  to: `${req.session.email}`,
  from: 'gohobbiesapp@gmail.com',
  subject: 'You have added a new hobby',
  text: `You have added a new hobby - ${req.body.title}`,
  html: `<strong><h1>Hello ${req.session.username}</h1><p>Thank you for using GoHobbies App.</p><p>You have successfully added a new hobby: 
                                    ${req.body.title}</p></strong>`,
};
sgMail.send(msg);

          client.messages
            .create({
                body: `Hello, user ${req.session.username}, you have added a new hobby - ${req.body.title}`,
                from: process.env.TWILIO_PHONE,
                to: '+2349084823977'
            })
            .then(message => console.log(message.sid))
            .done();
          }res.redirect('dashboard');
      })
  } else {
        message = 'Invalid Hobby'
    }
  }
}
